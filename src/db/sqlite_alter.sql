BEGIN;
CREATE TABLE "adminer_todolist" (
  "id" integer NULL PRIMARY KEY AUTOINCREMENT,
  "uuid" text NOT NULL,
  "parent_id" integer NOT NULL DEFAULT '0',
  "list_id" integer NOT NULL DEFAULT '0',
  "d_created" integer NOT NULL DEFAULT '0',
  "d_completed" integer NOT NULL DEFAULT '0',
  "d_edited" integer NOT NULL DEFAULT '0',
  "compl" integer NOT NULL DEFAULT '0',
  "title" text NOT NULL,
  "note" text NULL,
  "prio" integer NOT NULL DEFAULT '0',
  "ow" integer NOT NULL DEFAULT '0',
  "tags" text NOT NULL DEFAULT '',
  "tags_ids" text NOT NULL DEFAULT '',
  "duedate" numeric NULL
);
INSERT INTO "adminer_todolist" ("id", "uuid", "list_id", "d_created", "d_completed", "d_edited", "compl", "title", "note", "prio", "ow", "tags", "tags_ids", "duedate") SELECT "id", "uuid", "list_id", "d_created", "d_completed", "d_edited", "compl", "title", "note", "prio", "ow", "tags", "tags_ids", "duedate" FROM "todolist";
DROP TABLE "todolist";
ALTER TABLE "adminer_todolist" RENAME TO "todolist";
CREATE INDEX "todo_list_id" ON "todolist" ("list_id");
CREATE UNIQUE INDEX "todo_uuid" ON "todolist" ("uuid");
COMMIT;
