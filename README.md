myTinyTodo
==========

myTinyTodo is based on Max Pozdeev's myTinyTodo. Since development is
abbandoned there, a fork is created and further developed.

SQLITE UPDATE FOR TASK PARENTING:
===================================

BEGIN;
CREATE TABLE "adminer_todolist" (
  "id" integer NULL PRIMARY KEY AUTOINCREMENT,
  "uuid" text NOT NULL,
  "parent_id" integer NOT NULL DEFAULT '0',
  "list_id" integer NOT NULL DEFAULT '0',
  "d_created" integer NOT NULL DEFAULT '0',
  "d_completed" integer NOT NULL DEFAULT '0',
  "d_edited" integer NOT NULL DEFAULT '0',
  "compl" integer NOT NULL DEFAULT '0',
  "title" text NOT NULL,
  "note" text NULL,
  "prio" integer NOT NULL DEFAULT '0',
  "ow" integer NOT NULL DEFAULT '0',
  "tags" text NOT NULL DEFAULT '',
  "tags_ids" text NOT NULL DEFAULT '',
  "duedate" numeric NULL
);
INSERT INTO "adminer_todolist" ("id", "uuid", "list_id", "d_created", "d_completed", "d_edited", "compl", "title", "note", "prio", "ow", "tags", "tags_ids", "duedate") SELECT "id", "uuid", "list_id", "d_created", "d_completed", "d_edited", "compl", "title", "note", "prio", "ow", "tags", "tags_ids", "duedate" FROM "todolist";
DROP TABLE "todolist";
ALTER TABLE "adminer_todolist" RENAME TO "todolist";
CREATE INDEX "todo_list_id" ON "todolist" ("list_id");
CREATE UNIQUE INDEX "todo_uuid" ON "todolist" ("uuid");
COMMIT;


System requirements
-------------------
* PHP 5.2.0 or greater;
* PHP extensions: php_mysql (MySQL version), php_pdo and php_pdo_sqlite (SQLite version).
Tested browsers:
* Internet Explorer 8
* Firefox v3.6, v4
* Chrome v8
* Opera v10.60, v11
* Safari v5

How to install myTinyTodo
-------------------------
1. Download, unpack and upload to your site. 
2. Run setup.php, select and specify settings of database you prefer.
   For sqlite usage make sure that database file 'db/todolist.db' is writable for webserver/php.
   Then click "Install" to create tables in database. It's recommended to delete this file after installation.
3. To protect your tasks from modification by the others you may specify password in settings.<br>
   By default session files are stored in 'tmp/sessions' directory. Make sure it's writable for webserver/php.
4. Open 'index.php' in your browser to run the application. Then go to settings and specify your time zone.

Update to new version
---------------------
Download, unpack and replace all files excluding directory 'db'.
Run 'setup.php' and upgrade database if required.

How to show completed tasks
---------------------------
Use appropriate command in the tab menu.

How to hide a tab
-----------------
Hold the Ctrl (or Cmd) button and click on the tab.

How to show tasks from all tabs in one list
-------------------------------------------
There is the "Select List" menu to the right of tabs. 
Here you'll find all the tabs you have created. Hidden tabs too. And special tab with all tasks.

How to select the sorting in reverse order
------------------------------------------
You can toggle the order (ascending or descending) by clicking on the sorting menu item once more (see the arrow).